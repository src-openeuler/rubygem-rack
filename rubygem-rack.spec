%global gem_name rack
%bcond_with bootstrap

Name:    rubygem-%{gem_name}
Version: 2.2.4
Epoch:   1
Release: 9
Summary: A modular Ruby webserver interface
License: MIT and BSD
URL:     https://rack.github.io/
Source0: https://rubygems.org/downloads/%{gem_name}-%{version}.gem
Patch0: 2-2-multipart-dos.patch 
Patch1: 2-2-header-redos.patch
Patch2: Fix-CVE-2024-26141.patch
Patch3: Fix-CVE-2024-26146.patch
Patch4: Fix-CVE-2024-25126.patch
Patch5: Fix-CVE-2022-44570.patch
Patch6: Fix-CVE-2022-44571.patch
Patch7: Fix-CVE-2022-44572.patch
Patch8: Fix-CVE-2025-27610.patch
BuildRequires: ruby(release) rubygems-devel ruby >= 2.2.2 git
BuildRequires: memcached rubygem(memcache-client) rubygem(minitest)
BuildRequires: rubygem(memcache-client)
BuildRequires: rubygem(minitest)
%if ! %{with bootstrap}
BuildRequires: rubygem(thin)
%endif
BuildArch: noarch

%global __brp_mangle_shebangs_exclude_from ^%{gem_instdir}/test/cgi/test.ru$

%description
Rack provides a minimal, modular and adaptable interface for developing
web applications in Ruby.  By wrapping HTTP requests and responses in
the simplest way possible, it unifies and distills the API for web
servers, web frameworks, and software in between (the so-called
middleware) into a single method call.


%package help
Summary:   Documentation for %{name}
Requires:  %{name} = %{epoch}:%{version}-%{release}
Provides:  %{name}-doc = %{epoch}:%{version}-%{release}
Obsoletes: %{name}-doc < %{epoch}:%{version}-%{release}
BuildArch: noarch

%description help
Documentation for %{name}.

%prep
%autosetup -n %{gem_name}-%{version} -p1 -S git

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x
find %{buildroot}%{gem_instdir}/{bin,test/cgi} -type f | \
  xargs sed -i 's|^#!/usr/bin/env ruby$|#!/usr/bin/ruby|'

for file in `find %{buildroot}/%{gem_instdir} -type f -perm /a+x`; do
    [ -z "`head -n 1 $file | grep \"^#!/\"`" ] && chmod -v 644 $file
done

for file in `find %{buildroot}%{gem_instdir} -type f`; do
    [ ! -z "`head -n 1 $file | grep \"^#!\"`" ] && chmod -v 755 $file
done

%check
pushd .%{gem_instdir}
PID=%(mktemp)
memcached -d -P "$PID"
LC_ALL=C.UTF-8 \
ruby -Ilib:test -e 'Dir.glob "./test/spec_*.rb", &method(:require)'
kill -TERM $(< "$PID")
popd

%files
%dir %{gem_instdir}
%{_bindir}/rackup
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%{gem_instdir}/bin
%exclude %{gem_cache}
%{gem_spec}

%files help
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc
%doc %{gem_instdir}/CONTRIBUTING.md
%doc %{gem_instdir}/SPEC.rdoc
%{gem_instdir}/Rakefile
%{gem_instdir}/%{gem_name}.gemspec
%doc %{gem_instdir}/example
%doc %{gem_instdir}/contrib

%changelog
* Wed Mar 12 2025 changtao <changtao@kylinos.cn> - 1:2.2.4-9
- Type:CVE
- CVE:CVE-2025-27610
- SUG:NA
- DESC:fix CVE-2025-27610

* Thu Apr 11 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-8
- Type:CVES
- ID:CVE-2022-44572
- SUG:NA
- DESC:CVE-2022-44572

* Wed Apr 10 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-7
- Type:CVES
- ID:CVE-2022-44571
- SUG:NA
- DESC:CVE-2022-44571

* Tue Apr 09 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-6
- Type:CVES
- ID:CVE-2022-44570
- SUG:NA
- DESC:CVE-2022-44570

* Sun Apr 07 2024 panchenbo <panchenbo@kylinsec.com.cn> - 1:2.2.4-5
- Type: Bugfix
- ID:NA
- SUG:NA
- DESC:add BuildRequires: git

* Tue Apr 02 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-4
- Type:CVES
- ID:CVE-2024-25126
- SUG:NA
- DESC:CVE-2024-25126

* Tue Apr 02 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-3
- Type:CVES
- ID:CVE-2024-26146
- SUG:NA
- DESC:CVE-2024-26146

* Mon Apr 01 2024 zouzhimin <zouzhimin@kylinos.cn> - 1:2.2.4-2
- Type:CVES
- ID:CVE-2024-26141
- SUG:NA
- DESC:CVE-2024-26141

* Thu Aug 17 2023 wulei <wu_lei@hoperun.com> - 1:2.2.4-1
- Upgrade to version 2.2.4

* Tue Jun 13 2023 bizhiyuan <bizhiyuan@kylinos.cn> - 1:2.2.3.1-2
- Type:CVES
- ID:CVE-2023-27539 CVE-2023-27530
- SUG:NA
- DESC:Fix CVE-2023-27539 CVE-2023-27530

* Tue Jun 28 2022 wangkai <wangkai385@h-partners.com> - 1:2.2.3.1-1
- Upgrade to 2.2.3.1 for fix CVE-2020-8184 CVE-2022-30122 CVE-2022-30123

* Mon Mar 7 2022 duyiwei<duyiwei@kylinos.cn>- 1:2.2.2-2
- delete BuildRequires rubygem(concurrent-ruby)

* Mon Feb  8 2021 sunguoshuai<sunguoshuai@huawei.com>- 1:2.2.2-1
- Upgrade to 1:2.2.2

* Wed Sep 9 2020 Ge Wang <wangge20@huawei.com> - 1:2.0.4-2
- Modify Source0 Url

* Thu Mar 5 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:2.0.4-1
- Package init
